export default{
  jwt:{
    secret: process.env.JWT_SECRET || '9787922f286a4349a009237d0b2ffd73',
    expiresIn: process.env.EXPIRES_IN || '1d',
  }
}
