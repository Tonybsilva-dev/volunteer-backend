import { EntityRepository, Repository } from 'typeorm';
import Point from '../infra/typeorm/entities/Point';

@EntityRepository(Point)
class PointsRepository extends Repository<Point> {

  public async findByName(name: string): Promise<Point | null> {

    const findUser = await this.findOne({
      where: { name },
    });

    return findUser || null;
  }
}
export default PointsRepository;
