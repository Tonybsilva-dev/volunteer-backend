import { randomUUID } from "crypto";
import {
  Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn
} from 'typeorm';


@Entity('points')
class Point {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  email: string;

  @Column()
  whatsapp: string;

  @Column()
  image: string;

  @Column()
  lat: string;

  @Column()
  log: string;

  @Column()
  reference: string;

  @Column()
  city: string;

  @Column()
  uf: string;

  @Column()
  is_active: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  constructor() {
    if (!this.id) {
      this.id = randomUUID();
    }
  }
}

export default Point;
