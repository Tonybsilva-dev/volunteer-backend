import { CreatePointController } from './CreatePointController';
import { CreatePointUseCase } from './CreatePointUseCase';

const createPointUseCase = new CreatePointUseCase();

const createPointController = new CreatePointController(createPointUseCase);

export { createPointUseCase, createPointController };
