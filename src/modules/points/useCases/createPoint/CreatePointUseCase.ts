import Point from '@modules/points/infra/typeorm/entities/Point';
import AppError from '@shared/errors/AppError';
import { getRepository } from 'typeorm';
import { ICreatePointDTO } from './CreatePointDTO';

export class CreatePointUseCase {
  public async execute({ name,
    description,
    email,
    whatsapp,
    image,
    lat,
    log,
    reference,
    city,
    uf, }: ICreatePointDTO) {
    const pointRepository = getRepository(Point)
    const checkPointExists = await pointRepository.findOne({
      where: { email }
    })

    if (checkPointExists) {
      throw new AppError('Email address already used to create a annuncement.')
    }

    const point = pointRepository.create({
      name,
      description,
      email,
      whatsapp,
      image,
      lat,
      log,
      reference,
      city,
      uf,
      is_active: true
    })

    await pointRepository.save(point)

    return point
  }
}
