import { Request, Response } from 'express';
import { CreatePointUseCase } from '../createPoint/CreatePointUseCase';

export class CreatePointController {

  constructor(private createPointUseCase: CreatePointUseCase) {}

  async create(request: Request, response :Response) {

    const {  name,
      description,
      email,
      whatsapp,
      image,
      latitude,
      longitude,
      reference,
      city,
      uf, } = request.body;

    const createPoint = new CreatePointUseCase();

    const point = await createPoint.execute({
      name,
      description,
      email,
      whatsapp,
      image,
      lat: latitude,
      log: longitude,
      reference,
      city,
      uf,
    });

    return response.json(point)
  }
}
