export interface ICreatePointDTO {
  name: string;
  description: string;
  email: string;
  whatsapp : string;
  image ?: string;
  lat: string;
  log: string;
  reference?: string;
  city: string;
  uf: string;
}
