import { Request, Response, Router } from 'express';
import { createPointController } from '../useCases/createPoint';

const pointsRouter = Router();

pointsRouter.post('/', (request: Request, response: Response) => {
  return createPointController.create(request, response);
});


export default pointsRouter;
