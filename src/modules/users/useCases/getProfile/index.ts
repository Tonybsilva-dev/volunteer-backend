import { GetProfileUserController } from './getProfileUserController';
import { GetProfileUserUseCase } from './getProfileUserUseCase'

const getProfileUserUseCase = new GetProfileUserUseCase();

const getProfileUserController = new GetProfileUserController(getProfileUserUseCase)

export { getProfileUserUseCase, getProfileUserController };
