import AppError from '@shared/errors/AppError'
import { getRepository } from 'typeorm'
import User from '../../infra/typeorm/entities/User'
import { IGetProfileUserDTO } from './getProfileUserDTO'

export class GetProfileUserUseCase {
  public async execute({ user_id }: IGetProfileUserDTO): Promise<User> {

    const usersRepository = getRepository(User)

    const user = await usersRepository.findOne({
      where: {
        id: user_id
      }
    })

    if(!user){
      throw new AppError('User not found.', 401)
    }

    delete user.password;

    return user;
  }
}
