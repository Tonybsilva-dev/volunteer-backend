import { Request, Response } from 'express';
import { GetProfileUserUseCase } from './getProfileUserUseCase';

export class GetProfileUserController {

  constructor(private getProfileUserUseCase: GetProfileUserUseCase) { }

  async index( request: Request , response: Response){

    const user_id = request.user.id

    const getProfileUserUseCase = new GetProfileUserUseCase

    const user = await getProfileUserUseCase.execute({ user_id })

    return response.json(user);
  }
}
