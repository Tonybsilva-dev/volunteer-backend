import { UploadAvatarUserCase } from './UploadAvatarUserCase';
import { UploadAvatarUserController } from './UploadAvatarUserController';

const uploadAvatarUserCase = new UploadAvatarUserCase;

const uploadAvatarUserController = new UploadAvatarUserController(uploadAvatarUserCase);

export { uploadAvatarUserCase, uploadAvatarUserController };

