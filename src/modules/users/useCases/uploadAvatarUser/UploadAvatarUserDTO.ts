export interface IUploadAvatarUserDTO{
  user_id: string,
  avatarFileName: string
}
