import { Request, Response } from 'express';
import { UploadAvatarUserCase } from './UploadAvatarUserCase';

export class UploadAvatarUserController {

  constructor(private uploadAvatarUseCase: UploadAvatarUserCase) { }

  async upload( request: Request , response: Response) {

    const uploadAvatarUserCase = new UploadAvatarUserCase;

    const user = await uploadAvatarUserCase.execute({
      user_id: request.user.id,
      avatarFileName: request.file?.filename
    })

    delete user.password;

    return response.json(user);
  }
}
