import uploadConfig from '@config/upload';
import AppError from '@shared/errors/AppError';
import fs from 'fs';
import path from 'path';
import { getRepository } from 'typeorm';
import User from '../../infra/typeorm/entities/User';
import { IUploadAvatarUserDTO } from './UploadAvatarUserDTO';


export class UploadAvatarUserCase{
  public async execute({ user_id,  avatarFileName }: IUploadAvatarUserDTO): Promise<User>{

    const usersRepository = getRepository(User)

    const user = await usersRepository.findOne(user_id)

    if(!user){
      throw new AppError('Only authenticaded users can change avatar.', 401)
    }

    if(user.avatar){
      const userAvatarFilePath = path.join(uploadConfig.directory, user.avatar)

      const userAvatarFileExists = await fs.promises.stat(userAvatarFilePath)

      if(userAvatarFileExists){

        await fs.promises.unlink(userAvatarFilePath)
      }
    }

    user.avatar = avatarFileName;

    await usersRepository.save(user)

    return user;

  }
}
