import { Request, Response } from 'express';
import { CreateUserUseCase } from './CreateUserUseCase';

export class CreateUserController {

  constructor(private createUserUseCase: CreateUserUseCase) {}

  async create(request: Request, response :Response) {

    const { name, email, password } = request.body;

    const createUser = new CreateUserUseCase();

    const user = await createUser.execute({
        name,
        email,
        password,
    });

    delete user.password;

    return response.json(user)
  }
}
