import { DesactiveAccountController } from "./DesactiveAccountController";
import { DesactiveAccountUseCase } from "./DesactiveAccountUseCase";


const desactiveAccountUseCase = new DesactiveAccountUseCase();

const desactiveAccountController = new DesactiveAccountController(desactiveAccountUseCase)

export { desactiveAccountUseCase, desactiveAccountController };
