import { Request, Response } from 'express';
import { DesactiveAccountUseCase } from './DesactiveAccountUseCase';

export class DesactiveAccountController {

  constructor(private desactiveAccountUseCase: DesactiveAccountUseCase) {}

  async handle(request: Request, response :Response) {
    const user_id = request.user.id;

    const desactiveAccount = new DesactiveAccountUseCase();

    const user = await desactiveAccount.execute({
        id: user_id
    });

    return response.status(200).json(user);
  }
}
