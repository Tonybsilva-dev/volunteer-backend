import AppError from '@shared/errors/AppError'
import { getRepository } from 'typeorm'
import User from '../../infra/typeorm/entities/User'
import { ICreateDesactiveAccountDTO } from './DesactiveAccountDTO'

export class DesactiveAccountUseCase {
  public async execute({ id }: ICreateDesactiveAccountDTO): Promise<User> {
    const usersRepository = getRepository(User)

    const checkUserExists = await usersRepository.findOne({
      where: { id },
    });

    if (checkUserExists.is_active === false) {
      throw new AppError('Your account already desactived.')
    }

    const user = checkUserExists;

    user.is_active = false;

    return usersRepository.save(user)
  }
}


