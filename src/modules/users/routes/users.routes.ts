import UploadConfig from '@config/upload';
import ensureAuthenticated from '@shared/infra/http/middlewares/ensureAuthenticated';
import { Request, Response, Router } from 'express';
import multer from 'multer';
import { createUserController } from '../useCases/createUser';
import { desactiveAccountController } from '../useCases/desactiveAccount';
import { getProfileUserController } from '../useCases/getProfile';
import { uploadAvatarUserController } from '../useCases/uploadAvatarUser';

const usersRouter = Router();
const upload = multer(UploadConfig);

usersRouter.post('/', (request: Request, response: Response) => {
  return createUserController.create(request, response);
});

usersRouter.patch('/avatar', ensureAuthenticated, upload.single('avatar'),(request: Request, response: Response) => {
  return uploadAvatarUserController.upload(request, response);
});

usersRouter.get('/', ensureAuthenticated, (request: Request, response: Response) => {
  return getProfileUserController.index(request, response);
});

usersRouter.delete('/desactive', ensureAuthenticated, (request: Request, response: Response) => {
  return desactiveAccountController.handle(request, response);
});


export default usersRouter;
