import User from '../../users/infra/typeorm/entities/User';

export interface IAuthRequestDTO {
  email: string,
  password: string,
}

export interface IAuthResponseDTO{
  user: User,
  token: string,
}
