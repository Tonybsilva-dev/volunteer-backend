import AuthConfig from '@config/auth'
import AppError from '@shared/errors/AppError'
import { compare } from 'bcryptjs'
import { sign } from 'jsonwebtoken'
import { getRepository } from 'typeorm'
import User from '../../users/infra/typeorm/entities/User'
import { IAuthRequestDTO, IAuthResponseDTO } from './AuthUserDTO'


export class AuthUserUseCase {
  public async execute({ email, password }: IAuthRequestDTO): Promise<IAuthResponseDTO> {
    const usersRepository = getRepository(User)

    const user = await usersRepository.findOne({
      where: { email }
    })

    if (!user) {
      throw new AppError('Incorrect email/password combination.', 401)
    }

    const passwordMatched = await compare(password, user.password)

    if (!passwordMatched) {
      throw new AppError('Incorrect email/password combination.', 401)
    }

    const { secret, expiresIn } = AuthConfig.jwt

    const token = sign({}, secret, {

      subject: user.id,

      expiresIn: expiresIn,
    });

    return {
      user,
      token
    }

  }
}
