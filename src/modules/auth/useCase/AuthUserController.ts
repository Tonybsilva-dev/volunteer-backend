import { Request, Response } from 'express';
import { AuthUserUseCase } from './AuthUserUseCase';

export class AuthUserController {
  constructor(private authUserService: AuthUserUseCase) {}

  async handle(request:Request,response:Response){

    const { email, password } = request.body;

    const authenticatedUserService = new AuthUserUseCase();

    const result = await authenticatedUserService.execute({email, password})

    return response.json(result)
  }
}
