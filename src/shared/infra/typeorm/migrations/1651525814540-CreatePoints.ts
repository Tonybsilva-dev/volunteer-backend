import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreatePoints1651525814540 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
          new Table({
            name: 'points',
            columns: [
              {
                name: 'id',
                type: 'uuid',
                isPrimary: true,
                generationStrategy: 'uuid',
                default: 'uuid_generate_v4()',
              },
              {
                name: 'name',
                type: 'varchar',
              },{
                name: 'description',
                type: 'varchar',
              },
              {
                name: 'email',
                type: 'varchar',
                isUnique: true
              },
              {
                name: 'whatsapp',
                type: 'varchar',
                isNullable: false,
              },
              {
                name: 'image',
                type: 'varchar',
                isNullable: false
              },
              {
                name: 'lat',
                type: 'varchar',
              },
              {
                name: 'log',
                type: 'varchar',
              },
              {
                name: 'reference',
                type: 'varchar',
                isNullable: false
              },
              {
                name: 'city',
                type: 'varchar',
              },
              {
                name: 'uf',
                type: 'varchar',
              },
              {
                name: 'is_active',
                type: 'boolean',
              },
              {
                name: 'created_at',
                type: 'timestamp',
                default: 'now()',
              },
              {
                name: 'updated_at',
                type: 'timestamp',
                default: 'now()',
              },
            ]
          })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropTable('points')
    }

}
