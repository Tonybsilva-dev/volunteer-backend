import AppError from '@shared/errors/AppError';
import { createConnection } from 'typeorm';

const port = 5432

try {
  createConnection().then(
    () => console.log(`✅ Database connected on port ${port}!`)
  ); //Importa o ormconfig.json
} catch (error) {
    throw new AppError('Database not connected.', 500, error);
}

