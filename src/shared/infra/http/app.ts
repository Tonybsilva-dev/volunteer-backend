import cors from 'cors';
import express from 'express';
import 'express-async-errors';
import 'reflect-metadata';
import swaggerUi from "swagger-ui-express";
import UploadConfig from '../../../config/upload';
import swaggerFile from "../../docs/swagger.json";
import '../typeorm';
import globalError from './middlewares/globalError';
import { logRequests } from './middlewares/logRequests';
import routes from './routes';

const app = express();
app.use(cors())
app.use(express.json());
app.use(logRequests);
app.use('/files', express.static(UploadConfig.directory));
app.use('/api',routes);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerFile));
app.use(globalError);

export { app };
