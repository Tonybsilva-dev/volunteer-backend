
import { authRouter } from '@modules/auth/routes/auth.routes';
import pointsRouter from '@modules/points/routes';
import usersRouter from '@modules/users/routes/users.routes';
import { Router } from 'express';

const routes = Router();

routes.use('/users', usersRouter);
routes.use('/points', pointsRouter);
routes.use('/auth', authRouter)

export default routes;

