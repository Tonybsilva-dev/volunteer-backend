FROM node:14.16.0-alpine

ADD . /src

WORKDIR /usr/src/app

RUN rm -rf node_modules yarn.lock package-lock.json

COPY ./package*.json ./

RUN npm install yarn

RUN yarn

COPY . .

EXPOSE 5001

CMD npm run dev
